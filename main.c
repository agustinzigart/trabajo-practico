#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include "inc/menu.h"
#include "inc/MateriasFunciones.h"
#include "inc/ListaDeEstudiantes.h"

int main()
{
    ListaDeEstudiantes *listaDeEstudiantes = crearListaDeEstudiantes();
    ListaDeMaterias *listaDeMaterias = nuevaListaDeMaterias();
    while (1)
    {
        imprimirMenu(listaDeEstudiantes, listaDeMaterias);
    }
    return 0;
}
