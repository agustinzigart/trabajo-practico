$directorioActual = $PSScriptRoot

# Cambiar al directorio actual
Set-Location $directorioActual

# Comprobar si el cambio de directorio fue exitoso
if ($?) {
    # Compilar los archivos fuente usando gcc
    gcc main.c functions/ListaDeEstudiantes.c functions/menu.c functions/MateriasFunciones.c -o main

    # Comprobar si la compilación fue exitosa
    if ($?) {
        # Ejecutar el archivo generado
        .\main
    }
    else {
        Write-Host "Error: No se pudo compilar el programa."
    }
}
else {
    Write-Host "Error: No se pudo cambiar al directorio especificado."
}
