#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include "inc/menu.h"
#include "inc/MateriasFunciones.h"
#include "inc/ListaDeEstudiantes.h"
#include <unistd.h>

// -- Unit Test Functions declarations
static short int myAssertEqualsIntegers(int expected, int actual);
static short int myAssertStructNotNull(void *structPointer);
static short int myAssertStructNull(void *structPointer);
static short int myAssertTrue(int actualBoolean);
static short int myAssertFalse(int actualBoolean);

// -- Unit Tests declarations
void test1_crearEstudiantes(ListaDeEstudiantes *listaDeEstudiantes, ListaDeMaterias *listaDeMaterias);
void test2_buscarEstudiantePorNombre(ListaDeEstudiantes *listaDeEstudiantes, ListaDeMaterias *listaDeMaterias);
void test3(ListaDeEstudiantes *listaDeEstudiantes, ListaDeMaterias *listaDeMaterias);
void test4_darDeAltaMaterias(ListaDeEstudiantes *listaDeEstudiantes, ListaDeMaterias *listaDeMaterias);
void test5_anotarAMaterias(ListaDeEstudiantes *listaDeEstudiantes, ListaDeMaterias *listaDeMaterias);

// -- Unit Test Functions definitions
static short int myAssertEqualsIntegers(int expected, int actual){
    return (expected == actual);
}

static short int myAssertStructNotNull(void *structPointer){
    return (structPointer != NULL);
}

static short int myAssertStructNull(void *structPointer){
    return (structPointer == NULL);
}

static short int myAssertTrue(int actualBoolean){
    return (actualBoolean == 1);
}

static short int myAssertFalse(int actualBoolean){
    return (actualBoolean == 0);
}

// -- Unit Test definitions
void test1_crearEstudiantes(ListaDeEstudiantes *listaDeEstudiantes, ListaDeMaterias *listaDeMaterias){

    printf("------- TEST 1 - CREAR Y AGREGAR ESTUDIANTES -------");
    append(listaDeEstudiantes, crearEstudiante("Gaspar", 20));
    append(listaDeEstudiantes, crearEstudiante("Graiela", 32));
    append(listaDeEstudiantes, crearEstudiante("Mateo", 16));
    printf("Assert N1 largo de la lista: %i \n", myAssertEqualsIntegers(3, length(listaDeEstudiantes)));
    listarEstudiantes(listaDeEstudiantes);              // -- Printeamos lista actual
    return;
}

void test2_buscarEstudiantePorNombre(ListaDeEstudiantes *listaDeEstudiantes, ListaDeMaterias *listaDeMaterias){
    Estudiante *buscado;
    
    printf("------- TEST 2 - BUSCAR ESTUDIANTES -------");
    // -- Agregamos algunos estudiantes mas
    append(listaDeEstudiantes, crearEstudiante("Agustin", 21));
    append(listaDeEstudiantes, crearEstudiante("Franco", 26));
    append(listaDeEstudiantes, crearEstudiante("David", 29));
    listarEstudiantes(listaDeEstudiantes); 
    buscado = buscarEstudiante(listaDeEstudiantes, "Agustin");
    printf("Assert N1 buscar estudiante: %i \n", myAssertStructNotNull(buscado));
    buscado = buscarEstudiante(listaDeEstudiantes, "Agus");
    printf("Assert N2 buscar estudiante: %i \n", myAssertStructNull(buscado));
    return;
}

void test3_crearEstudiantesAleatorios(ListaDeEstudiantes *listaDeEstudiantes, ListaDeMaterias *listaDeMaterias){
    printf("------- TEST 3 - CREAR ESTUDIANTES ALEATORIOS -------");
    int largoLista = length(listaDeEstudiantes);
    generarEstudiantesAleatorios(5, listaDeEstudiantes); // -- Genero 10 estudiantes aleatorios
    largoLista += 5 ;
    printf("Assert N1 largo de la lista generados 10 estudiantes aleatorios: %i \n", myAssertEqualsIntegers(largoLista, length(listaDeEstudiantes)));
    listarEstudiantes(listaDeEstudiantes);
    append(listaDeEstudiantes, crearEstudiante("Steave Ray Vaughan", 36));
    append(listaDeEstudiantes, crearEstudiante("Sid Vicious", 21));
    largoLista += 2 ;
    printf("Assert N2 largo de la lista agregados otros 2 estudiantes: %i \n", myAssertEqualsIntegers(largoLista, length(listaDeEstudiantes)));
    return;   
}

void test4_darDeAltaMaterias(ListaDeEstudiantes *listaDeEstudiantes, ListaDeMaterias *listaDeMaterias){
    int i;
    int largoLista = 0, materiasAleatAgregadas = 0;
    short int ret;
    
    printf("------- TEST 4 - DAR DE ALTA MATERIAS EN LA CARRERA -------");
    
    listaMateriasCarrera(listaDeMaterias);
    
    ret = darAltaMateria(listaDeMaterias, "FISICA 1");
    if(ret == 1){largoLista ++;}
    printf("Assert N1 chequear largo luego de agregar 1 materia: %i \n", myAssertEqualsIntegers(largoLista, lengthMateria(listaDeMaterias)));
    
    for(i = 0; i < 5; i++){
        ret = generarMateriaAleatoria(listaDeMaterias);
        sleep(1);
        if(ret == 1)
            materiasAleatAgregadas ++;
        printf("Materias agregadas: %d\n", materiasAleatAgregadas);
    }
    largoLista += materiasAleatAgregadas;
    printf("Assert N2 chequear largo luego de generar 5 materias aleatorias, teniendo en cuenta posibles fallos debido a materias repetidas: %i \n", myAssertEqualsIntegers(largoLista, lengthMateria(listaDeMaterias)));
    
    listaMateriasCarrera(listaDeMaterias);
    
    return;
}

void test5_anotarAMaterias(ListaDeEstudiantes *listaDeEstudiantes, ListaDeMaterias *listaDeMaterias){
    short int ret = 0;
    Estudiante *estBuscado = NULL;
    Materia *matBuscada = NULL;
    char nombreMateria[30];
    int estudianteAnotado = 0;
    int verifCorrelat = 0;

    printf("------- TEST 5 - ANOTAR ESTUDIANTE A MATERIA -------\n");
    append(listaDeEstudiantes, crearEstudiante("Agustin", 12));
    listarEstudiantes(listaDeEstudiantes);
    darAltaMateria(listaDeMaterias, "ALGEBRA 1");
    ret = generarMateriaAleatoria(listaDeMaterias);
    ret = generarMateriaAleatoria(listaDeMaterias);
    listaMateriasCarrera(listaDeMaterias);
    estBuscado = buscarEstudiante(listaDeEstudiantes, "Agustin"); // -- Se busca alumno que se sabe que existe
    printf("Assert N1 buscar estudiante: %i \n", myAssertStructNotNull(estBuscado));

    strcpy(nombreMateria,"MOTORES 1");
    matBuscada = BuscarMateria(listaDeMaterias, nombreMateria);
    printf("Assert N2 buscar materia no existente en la carrera: %i \n", myAssertStructNull(matBuscada));
    
    memset(nombreMateria, 0x00, sizeof(nombreMateria));
    strcpy(nombreMateria,"ALGEBRA 1");
    matBuscada = BuscarMateria(listaDeMaterias, nombreMateria);
    printf("Assert N3 buscar materia existente en la carrera: %i \n", myAssertStructNotNull(matBuscada));
    
    // -- Anotar alumno a materia
    estudianteAnotado = estudianteEstaAnotado(estBuscado, nombreMateria);
    printf("Assert N4 el alumno no esta anotado en la materia: %i \n", myAssertFalse(estudianteAnotado));
    verifCorrelat = verificarCorrelativas(estBuscado, nombreMateria);
    printf("Assert N5 el alumno cumple con las correlatividades correspondientes: %i \n", myAssertTrue(verifCorrelat));

    if ((matBuscada != NULL) && (estBuscado != NULL)){ // ANALISIS MATEMATICO 1 , ALGEBRA 1
        if (!estudianteAnotado){   
            if(verifCorrelat){
                agregarMateria(estBuscado->materias, estBuscado);
            }
            else{
                printf("No se cumple la correlatividad\n");
            }
        }
    }
    printf("Assert N6 el alumno debe estar anotado ahora a 1 materia: %i \n", myAssertEqualsIntegers(1, length(estBuscado->materias)));
    
    return;
}

int main(){
    ListaDeEstudiantes *listaDeEstudiantes = crearListaDeEstudiantes();
    ListaDeMaterias *listaDeMaterias = nuevaListaDeMaterias();

    // test1_crearEstudiantes(listaDeEstudiantes, listaDeMaterias);
    // test2_buscarEstudiantePorNombre(listaDeEstudiantes, listaDeMaterias);
    // test3_crearEstudiantesAleatorios(listaDeEstudiantes, listaDeMaterias);
    // test4_darDeAltaMaterias(listaDeEstudiantes, listaDeMaterias);
    test5_anotarAMaterias(listaDeEstudiantes, listaDeMaterias);
    return 0;
}
