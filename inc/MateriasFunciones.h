/**
 *	@file MateriasFunciones.h
 *	@brief Functions to manage subjects (Materia structs)
 *
 *	@author FizzBuzz Group
 */

#ifndef MATERIASFUNCIONES_H_
#define MATERIASFUNCIONES_H_

/******************************************************************************
 * Includes
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
/******************************************************************************
 * Macros and constants
 ******************************************************************************/

/******************************************************************************
 * External Data types
 ******************************************************************************/
typedef struct Materia
{
    char nombre[100];
    int nota;
    struct Materia *proximaMateria;
} Materia;

typedef struct ListaDeMaterias
{
    Materia *head;
    Materia *tail;
    int length;
} ListaDeMaterias;

/******************************************************************************
 * External data declaration
 ******************************************************************************/

/******************************************************************************
 * External functions declaration
 ******************************************************************************/
short int darAltaMateria(ListaDeMaterias *listaDeMaterias, char *nombre);
ListaDeMaterias *nuevaListaDeMaterias();
void agregarMateria(ListaDeMaterias *lista, Materia *materia);
int lengthMateria(ListaDeMaterias *ListaDeMaterias);
Materia *BuscarMateria(ListaDeMaterias *listaDeMaterias, char *nombre);
void listaMateriasCarrera(ListaDeMaterias *listaMateria);
short int generarMateriaAleatoria(ListaDeMaterias *listaDeMaterias);
void mostrarArbolCorrelativas();
#endif /* MATERIASFUNCIONES_H_ */

/******************************** END OF FILE ********************************/
