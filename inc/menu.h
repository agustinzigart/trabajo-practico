/**
 *	@file menu.h
 *	@brief Menu Functions
 *
 *	
 *	@author FizzBuzz Group 
 */

#ifndef MENU_H_
#define MENU_H_

/******************************************************************************
* Includes
******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "ListaDeEstudiantes.h"
/******************************************************************************
* Macros and constants
******************************************************************************/

/******************************************************************************
* External Data types
******************************************************************************/

/******************************************************************************
* External data declaration
******************************************************************************/

/******************************************************************************
* External functions declaration
******************************************************************************/
void imprimirMenu();
void imprimirTitulo();
void imprimirCabecera();
void imprimirPieDePagina();
void opciones();
void limpiarConsola();
void pausar();
void opcionesDeEstudiantes(ListaDeEstudiantes *listaEstudiantes, ListaDeMaterias *listaDeMaterias);
void opcionesDeMaterias(ListaDeMaterias *listaDeMaterias, ListaDeEstudiantes *listaDeEstudiantes);
void opcionesEstadisticas(ListaDeMaterias *listaDeMaterias, ListaDeEstudiantes *listaDeEstudiantes);
#endif /* MENU_H_ */

/******************************** END OF FILE ********************************/
