/**
 *	@file ListaDeEstudiantes.h
 *	@brief Functions to manage students (Estudiante structs)
 *
 *	@author FizzBuzz Group
 */

#ifndef LISTADEESTUDIANTES_H_
#define LISTADEESTUDIANTES_H_

/******************************************************************************
 * Includes
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "MateriasFunciones.h"
/******************************************************************************
 * Macros and constants
 ******************************************************************************/
#define BOOL_MESSAGES {"FALSO", "VERDADERO"}
/******************************************************************************
 * External Data types
 ******************************************************************************/
typedef enum
{
    false,
    true
} bool;

typedef struct Estudiante
{
    char nombre[100];
    int edad;
    ListaDeMaterias *materias;
    struct Estudiante *next;
} Estudiante;

typedef struct ListaDeEstudiantes
{
    Estudiante *head;
    Estudiante *tail;
    int length;
} ListaDeEstudiantes;
/******************************************************************************
 * External data declaration
 ******************************************************************************/

/******************************************************************************
 * External functions declaration
 ******************************************************************************/
ListaDeEstudiantes *crearListaDeEstudiantes();
Estudiante *crearEstudiante(const char *nombre, int edad);
void append(ListaDeEstudiantes *listaDeEstudiantes, Estudiante *estudiante);
void removeEstudiante(ListaDeEstudiantes *listaDeEstudiantes, Estudiante *estudiante);
int length(ListaDeEstudiantes *listaDeEstudiantes);
Estudiante *buscarEstudiante(ListaDeEstudiantes *listaDeEstudiantes, const char *nombre);
void listarEstudiantes(ListaDeEstudiantes *listaDeEstudiantes);
Estudiante *buscarEstudianteEdad(ListaDeEstudiantes *listaDeEstudiantes, int edadMin, int edadMax);
void ordenarNombres(ListaDeEstudiantes *listaDeEstudiantes, int orden);
void ordenarEdad(ListaDeEstudiantes *listaDeEstudiantes, int orden);
ListaDeMaterias *averiguarListaDeMateriasDelEstudiante(Estudiante *estudiante);
void imprimirMateriasDeUnEstudiante(Estudiante *estudiante);
void generarEstudiantesAleatorios(int cantidadEstudiantes, ListaDeEstudiantes *lista);
int verificarCorrelativas(Estudiante* estudiante, const char* nombreMateria);
int estudianteEstaAnotado(Estudiante* estudiante, const char* nombreMateria);
int cantidadDeAnotadosEnUnaMateria(ListaDeEstudiantes*listaDeEstudiantes, char* nombreDeMateria);
int cantidadDeAprobadosEnUnaMateria(ListaDeEstudiantes *listaDeEstudiantes, char *nombreDeMateria);
int cantidadDeDesaprobadosEnUnaMateria(ListaDeEstudiantes *listaDeEstudiantes, char *nombreDeMateria);
float promedioDeNotasDeUnaMateria(ListaDeEstudiantes *listaDeEstudiantes, char *nombreDeMateria);
float promedioDeUnEstudiante(ListaDeEstudiantes *listaDeEstudiantes, char *nombreDeMateria);
#endif /* LISTADEESTUDIANTES_H_ */

/******************************** END OF FILE ********************************/
