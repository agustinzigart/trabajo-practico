#include "../inc/MateriasFunciones.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include "../inc/ListaDeEstudiantes.h"
short int darAltaMateria(ListaDeMaterias *listaDeMaterias, char *nombre)
{
    Materia *nuevaMateria = malloc(sizeof(Materia));
    short int ret = 0;

    strupr(nombre);

    Materia *aux = listaDeMaterias->head;
    int encontrado = 0;
    while (aux != NULL)
    {
        if (strcmp(nombre,aux->nombre)==0)
        {
            encontrado = 1;
            printf("La materia ya esta en la carrera\n");
            break;
        }
        aux = aux->proximaMateria;
    }

    if (encontrado == 0)
    {
        strcpy(nuevaMateria->nombre, nombre);
        nuevaMateria->nota = 0;
        nuevaMateria->proximaMateria = NULL;
        agregarMateria(listaDeMaterias, nuevaMateria);
        ret = 1;
    }
    return ret;
}

ListaDeMaterias *nuevaListaDeMaterias()
{
    ListaDeMaterias *nuevaLista = malloc(sizeof(ListaDeMaterias));
    nuevaLista->head = NULL;
    nuevaLista->tail = NULL;
    nuevaLista->length = 0;
    return nuevaLista;
}

void agregarMateria(ListaDeMaterias *lista, Materia *materia)
{
    if (lista->head == NULL)
    {
        lista->head = materia;
        lista->tail = materia;
        lista->length++;
        return;
    }
    lista->tail->proximaMateria = materia;
    lista->tail = materia;
    lista->length++;
}

void rendirMateria(Materia *materia, int nota)
{
    if (nota < 1 || nota > 10)
    {
        return;
    }
    materia->nota = nota;
}

int lengthMateria(ListaDeMaterias *ListaDeMaterias)
{
    return ListaDeMaterias->length;
}

Materia *BuscarMateria(ListaDeMaterias *listaDeMaterias, char *nombre)
{
    if (lengthMateria(listaDeMaterias) == 0)
    {
        return NULL;
    }

    Materia *aux = listaDeMaterias->head;

    int i;
    for (i = 0; i < strlen(nombre); i++)
    { // Guarda en Mayus los nombres ingresados, para tenerlo como regla de busqueda.
        nombre[i] = toupper(nombre[i]);
    }

    while (aux != NULL)
    {
        if (strcmp(aux->nombre, nombre) == 0)
        {
            return aux;
        }
        aux = aux->proximaMateria;
    }

    return NULL;
}

// Generar un nombre de materia aleatorio
short int generarMateriaAleatoria(ListaDeMaterias *listaDeMaterias)
{
    short int ret = 0;
    srand(time(NULL));
    char *materias[] = {"ANALISIS MATEMATICO 1", "FISICA 1", "ANALISIS MATEMATICO 2", "ALGEBRA 1", "ALGEBRA 2", "DISENIO LOGICO",
                        "CULTURA", "PROBLEMATICA", "MATEMATICA DISCRETA", "SISTEMAS OPERATIVOS AVANZADOS", "ALGORITMOS 1", "ALGORITMOS 2", "ALGORITMOS 3",
                        "ARQUITECTURA", "ESTRUCTURA DE DATOS", "MATEMATICAS ESPECIALES", "FISICA 2", "HISTORIA", "CUESTIONES"};
    const int numMaterias = sizeof(materias) / sizeof(materias[0]);
    int indiceAleatorio = rand() % numMaterias;
    ret = darAltaMateria(listaDeMaterias, materias[indiceAleatorio]);
    return ret;
}

// Muestra las materias de la carrera
void listaMateriasCarrera(ListaDeMaterias *listaMateria)
{
    int pagina = 1;
    int materiasMostradas = 0;
    Materia *actual = listaMateria->head;
    while (actual != NULL)
    {
        printf("Pagina %d\n", pagina);
        printf("-------------------\n");
        // Mostrar las siguientes 10 materias o menos si no quedan suficientes
        for (int i = 0; i < 10 && actual != NULL; i++)
        {
            printf(" %c %s", 254, actual->nombre);
            printf("\n");
            actual = actual->proximaMateria;
            materiasMostradas++;
        }

        // Verificar si hay más materias en la lista
        if (actual != NULL)
        {
            char opcion;
            printf("\nMostrar las siguientes 10 materias? (s/n): ");
            scanf(" %c", &opcion);

            if (opcion != 's' && opcion != 'S')
            {
                break;
            }
        }

        pagina++;
    }
    printf("\n-------Fin De Pagina-------\n");
    printf("Total de materias mostradas: %d\n", materiasMostradas);
}

//imprime el arbol de correlativas segun las materias previamente cargadas
void mostrarArbolCorrelativas()
{
    printf("----- Arbol de Correlativas -----\n");
    printf("\n");
    printf("ANALISIS MATEMATICO 1\n");
    printf("|-- ALGEBRA 2\n");
    printf("|   |-- ALGEBRA 1\n");
    printf("\n");
    printf("FISICA 1\n");
    printf("|-- ANALISIS MATEMATICO 1\n");
    printf("\n");
    printf("ANALISIS MATEMATICO 2\n");
    printf("|-- ALGEBRA 2\n");
    printf("|   |-- ALGEBRA 1\n");
    printf("|-- ANALISIS MATEMATICO 1\n");
    printf("\n");
    printf("ALGEBRA 2\n");
    printf("|-- ALGEBRA 1\n");
    printf("\n");
    printf("DISENIO LOGICO\n");
    printf("|-- ALGEBRA 1\n");
    printf("|-- SISTEMAS DE REPRESENTACION\n");
    printf("\n");
    printf("MATEMATICA DISCRETA\n");
    printf("|-- ALGEBRA 2\n");
    printf("\n");
    printf("ESTRUCTURA DE DATOS\n");
    printf("|-- ALGORITMOS 2\n");
    printf("\n");
    printf("MATEMATICAS ESPECIALES\n");
    printf("|-- ANALISIS MATEMATICO 2\n");
    printf("\n");
    printf("FISICA 2\n");
    printf("|-- ANALISIS MATEMATICO 2\n");
    printf("|-- FISICA 1\n");
    printf("\n");
    printf("ARQUITECTURA\n");
    printf("|-- DISENIO LOGICO\n");
    printf("\n");
    printf("ALGORITMOS 3\n");
    printf("|-- ESTRUCTURA DE DATOS\n");
    printf("\n");
    printf("CULTURA\n");
    printf("CUESTIONES\n");
    printf("HISTORIA\n");
    printf("PROBLEMATICAS\n");
    printf("ALGEBRA 1\n");
    printf("ANALISIS MATEMATICO 1\n");
    printf("ALGORITMOS 1\n");
    printf("SISTEMAS DE REPRESENTACION\n");
    printf("\n");
}