#include "../inc/menu.h"
#include <time.h>
#include <ctype.h>
#include <string.h>
#include "../inc/ListaDeEstudiantes.h"
void imprimirMenu(ListaDeEstudiantes *listaEstudiantes, ListaDeMaterias *listaDeMaterias)
{
    limpiarConsola();
    imprimirTitulo();
    printf("\n");
    opciones(listaEstudiantes, listaDeMaterias);
}

void imprimirTitulo()
{
    printf("  _____ ___ ______________  _   _  ___________ \n");
    printf(" |  ___|_ _|__  /__  / __ )| | | | |___ /___ / \n");
    printf(" | |_   | |   / /  / / |_||| | | |   / /  / / \n");
    printf(" | |_   | |  / /  / /|  _ || | | |  / /  / / \n");
    printf(" |  _|  | | / /_ / /_| |_) | |_| | / /_ / /_ \n");
    printf(" |_|   |___/____/____|____/|_____|/____/____| \n");
}

void imprimirCabecera()
{
    printf("#---------------------MENU---------------------#\n");
}

void imprimirPieDePagina()
{
    printf("#----------------------------------------------#\n");
}

void opciones(ListaDeEstudiantes *listaEstudiantes, ListaDeMaterias *listaDeMaterias)
{
    imprimirCabecera();
    printf("#  1. Opciones de Estudiantes                  #\n");
    printf("#  2. Opciones de Materia                      #\n");
    printf("#  3. Estadisticas                             #\n");
    imprimirPieDePagina();
    printf("\n");
    printf("Seleccione una opcion: ");
    int opcion;
    scanf("%d", &opcion);

    switch (opcion)
    {
    case 1:
        limpiarConsola();
        opcionesDeEstudiantes(listaEstudiantes, listaDeMaterias);
        break;
    case 2:
        limpiarConsola();
        opcionesDeMaterias(listaDeMaterias, listaEstudiantes);
        break;
    case 3:
        limpiarConsola();
        opcionesEstadisticas(listaDeMaterias, listaEstudiantes);
    
        break;
    default:
        printf("\nOpcion invalida, ingrese nuevamente una opcion \n");
        pausar();
        break;
    }
}

void limpiarConsola()
{
    system("cls");
}

void pausar()
{
    system("pause");
}

void opcionesDeEstudiantes(ListaDeEstudiantes *listaEstudiantes, ListaDeMaterias *listaDeMaterias)
{
    char nombre[100];
    Estudiante *estudianteEncontrado;
    int opcion;
    int orden;
    int cantidadDeEstudiantesRandom;
    int edadMin;
    int edadMax;
    char materiaNombre[100];
    Materia *materia;
    int nota;
    imprimirCabecera();
    printf("# 1. Crear estudiante                          #\n");
    printf("# 2. Crear n estudiantes aleatorios            #\n");
    printf("# 3. Listar estudiantes                        #\n");
    printf("# 4. Buscar estudiante por nombre              #\n");
    printf("# 5. Buscar estudiante por rango de edad       #\n");
    printf("# 6. Anotar estudiante a una materia           #\n");
    printf("# 7. Asignar nota a estudiante de una materia  #\n");
    printf("# 8. Ordenar estudiantes por nombre            #\n");
    printf("# 9. Ordenar estudiantes por edad              #\n");
    printf("# 0. Volver al menu principal                  #\n");

    imprimirPieDePagina();

    printf("Seleccione una opcion: ");
    scanf("%d", &opcion);

    switch (opcion)
    {
    case 0:
        break;
    case 1:
        limpiarConsola();
        printf("#--------------Crear estudiante--------------#\n");
        printf("Ingresa el nombre: ");
        scanf(" %s", &nombre);
        printf("Ingresa la edad: ");
        int edad;
        scanf(" %i", &edad);
        Estudiante *estudiante = crearEstudiante(nombre, edad);
        append(listaEstudiantes, estudiante);
        break;
    case 2:
        limpiarConsola();
        int cantidad;
        printf("Ingrese la cantidad de estudiantes aleatorios que desea generar: ");
        scanf("%d", &cantidad);
        generarEstudiantesAleatorios(cantidad, listaEstudiantes);
        listarEstudiantes(listaEstudiantes);
        printf("\n");
        pausar();
        break;
    case 3:
        limpiarConsola();
        printf("Los estudiantes son: \n");
        listarEstudiantes(listaEstudiantes);
        printf("\n");
        pausar();
        break;
    case 4:
        printf("Ingresa el nombre: ");
        scanf(" %s", &nombre);
        estudianteEncontrado = buscarEstudiante(listaEstudiantes, nombre);
        limpiarConsola();
        if (estudianteEncontrado != NULL)
        {
            imprimirCabecera();
            printf(" Nombre: %s \n", estudianteEncontrado->nombre);
            printf(" Edad: %i \n", estudianteEncontrado->edad);
            imprimirMateriasDeUnEstudiante(estudianteEncontrado);
            imprimirPieDePagina();
        }
        else
        {
            printf("Estudiante no encontrado.\n");
        }
        pausar();
        break;
    case 5:
        limpiarConsola();
        printf("Ingrese un valor minimo para el rango de edad: ");
        scanf("%i", &edadMin);
        printf("\nIngrese un valor maximo para el rango de edad: ");
        scanf("%i", &edadMax);
        printf("\n");
        buscarEstudianteEdad(listaEstudiantes, edadMin, edadMax);
        printf("\n");
        pausar();
        break;

    case 6:
        printf("Ingresa el nombre del estudiante: ");
        scanf(" %s", &nombre);
        strupr(nombre);
        estudianteEncontrado = buscarEstudiante(listaEstudiantes, nombre);
        listaMateriasCarrera(listaDeMaterias);
        printf("Ingresa el nombre de la materia: ");
        scanf(" %[^\n]", &materiaNombre);
        strupr(materiaNombre);
        materia = BuscarMateria(listaDeMaterias, materiaNombre);
        if (materia == NULL)
        {
            printf("No se encontro la materia");
        }
        else
        {
            if (estudianteEncontrado != NULL)
            {
                if (estudianteEstaAnotado(estudianteEncontrado, materiaNombre) == 1)
                {
                    printf("El estudiante ya esta anotado en la materia\n");
                }
                else if (verificarCorrelativas(estudianteEncontrado, materiaNombre) == 1)
                {
                    Materia *copiaMateria = (Materia *)malloc(sizeof(Materia));
                    strcpy(copiaMateria->nombre, materiaNombre);
                    copiaMateria->nota = 0; 
                    agregarMateria(estudianteEncontrado->materias, copiaMateria);
                }
                else
                {
                    printf("No se cumple la correlatividad\n");
                }
            }
            else
            {
                printf("No se encontro el estudiante\n");
            }
        }
        pausar();
        break;
    case 7:
        printf("Ingresa el nombre del estudiante: ");
        scanf(" %s", &nombre);
        strupr(nombre);
        estudianteEncontrado = buscarEstudiante(listaEstudiantes, nombre);
        if (estudianteEncontrado == NULL)
        {
            printf("No se encontro el estudiante");
            break;
        }
        ListaDeMaterias* listaDeMateriasEstudiante = estudianteEncontrado->materias;
        listaMateriasCarrera(listaDeMateriasEstudiante);
        printf("Ingresa el nombre de la materia: ");
        scanf(" %[^\n]", &materiaNombre);
        strupr(materiaNombre);
        materia = BuscarMateria(estudianteEncontrado->materias, materiaNombre);
        if (materia == NULL)
        {
            printf("No se encontro la materia");
            break;
        }
        printf("Ingresa la nota de la materia del 1 al 10: ");
        scanf(" %i", &nota);

        rendirMateria(materia, nota);
        break;
    case 8:
        limpiarConsola();
        printf("Ingrese 0 para orden Descendente, 1 para orden Ascendente: ");
        scanf("%i", &orden);
        if (orden == 0)
        {
            printf("Lista ordenada de forma Descendente:\n");
            ordenarNombres(listaEstudiantes, 0);
            listarEstudiantes(listaEstudiantes);
            pausar();
            break;
        }
        else if (orden == 1)
        {
            printf("Lista ordenada de forma Ascendente:\n");
            ordenarNombres(listaEstudiantes, 1);
            listarEstudiantes(listaEstudiantes);
            pausar();
            break;
        }
        else
        {
            printf("Opcion invalida.");
        }
    case 9:
        limpiarConsola();
        printf("Ingrese 0 para orden Descendente, 1 para orden Ascendente: ");
        scanf("%i", &orden);
        if (orden == 0)
        {
            printf("Lista ordenada de forma Descendente:\n");
            ordenarEdad(listaEstudiantes, 0);
            listarEstudiantes(listaEstudiantes);
            pausar();
            break;
        }
        else if (orden == 1)
        {
            printf("Lista ordenada de forma Ascendente:\n");
            ordenarEdad(listaEstudiantes, 1);
            listarEstudiantes(listaEstudiantes);
            pausar();
            break;
        }
        else
        {
            printf("Opcion invalida.");
        }
    default:
        printf("\nOpcion invalida, ingrese nuevamente una opcion \n");
        pausar();
        break;
    }
}

void opcionesDeMaterias(ListaDeMaterias *listaMaterias, ListaDeEstudiantes *listaDeEstudiantes)
{
    imprimirCabecera();
    printf("# 1. Lista de materias de la Carrera           #\n");
    printf("# 2. Agregar nueva materia a la Carrera        #\n");
    printf("# 3. Agregar materia aleatoria                 #\n");
    printf("# 4. Mostrar arbol de correlativas             #\n");
    printf("# 0. Volver al menu principal                  #\n");
    imprimirPieDePagina();
    int opcion;
    printf("Seleccione una opcion: ");
    scanf("%d", &opcion);
    char materiaNombre[100];
    Estudiante *estudianteEncontrado;
    Materia *materia;

    switch (opcion)
    {
    case 1:
        limpiarConsola();
        listaMateriasCarrera(listaMaterias);
        pausar();
        break;
    case 2:
        limpiarConsola();
        printf("Ingrese el nombre de la materia a dar de alta: ");
        scanf(" %[^\n]", &materiaNombre);
        darAltaMateria(listaMaterias, materiaNombre);
        pausar();
        break;
    case 3:
        limpiarConsola();
        generarMateriaAleatoria(listaMaterias);
        listaMateriasCarrera(listaMaterias);
        break;
    case 4:
        mostrarArbolCorrelativas();
        pausar();
        break;
    case 0:
        break;
    default:
        printf("\nOpcion invalida, ingrese nuevamente una opcion \n");
        pausar();
        break;
    }
}

void opcionesEstadisticas(ListaDeMaterias *listaDeMaterias, ListaDeEstudiantes *listaDeEstudiantes)
{
    imprimirCabecera();
    printf("#  1. Cantidad de anotados en una materia      #\n");
    printf("#  2. Cantidad de aprobados en una materia     #\n");
    printf("#  3. Cantidad de desaprobados en una materia  #\n");
    printf("#  4. Promedio de notas de una materia         #\n");
    printf("#  5. Promedio de notas de un estudiante       #\n");
    printf("#  0. Volver al menu principal                 #\n");
    imprimirPieDePagina();
    printf("\n");
    printf("Seleccione una opcion: ");
    int opcion;
    char materiaNombre[100];
    char nombre[100];
    scanf("%d", &opcion);
    switch (opcion)
    {
    case 0:
        break;
    case 1:
        limpiarConsola();
        printf("Ingrese el nombre de la materia: ");
        scanf(" %[^\n]", &materiaNombre);
        int anotados = cantidadDeAnotadosEnUnaMateria(listaDeEstudiantes, materiaNombre);
        printf("Hay %i anotado/s a %s \n", anotados,materiaNombre);
        pausar();
        break;
    case 2:
        limpiarConsola();
        printf("#-----Cantidad de aprobados en una materia-----#\n");
        printf("Ingrese el nombre de la materia: ");
        scanf(" %[^\n]", &materiaNombre);
        limpiarConsola();
        printf("#-----Cantidad de aprobados en una materia-----#\n");
        int aprobados = cantidadDeAprobadosEnUnaMateria(listaDeEstudiantes, materiaNombre);
        printf("Hay %i aprobado/s en %s \n", aprobados,materiaNombre);
        pausar();
        break;
    case 3:
        limpiarConsola();

        printf("#----Cantidad de desaprobados en una materia---#\n");
        printf("Ingrese el nombre de la materia: ");
        scanf(" %[^\n]", &materiaNombre);
        limpiarConsola();
        printf("#----Cantidad de desaprobados en una materia---#\n");
        int desaprobados = cantidadDeDesaprobadosEnUnaMateria(listaDeEstudiantes, materiaNombre);
        printf("Hay %i desaprobado/s en %s \n", desaprobados,materiaNombre);
        pausar();
        break;
    case 4: 
        limpiarConsola();
        printf("#-------Promedio de notas de una materia-------#\n");
        printf("Ingrese el nombre de la materia: ");
        scanf(" %[^\n]", &materiaNombre);
        limpiarConsola();
        printf("#-------Promedio de notas de una materia-------#\n");
        float promedio = promedioDeNotasDeUnaMateria(listaDeEstudiantes, materiaNombre);
        printf("El promedio para %s es de:  %.2f \n", materiaNombre, promedio);
        pausar();
        break;
    case 5: 
        limpiarConsola();
        printf("#-------Promedio de notas de un estudiante-----#\n");
        printf("Ingrese el nombre del estudiante: ");
        scanf(" %[^\n]", &nombre);
        limpiarConsola();
        printf("#-------Promedio de notas de un estudiante-----#\n");
        float promedioEstudiante = promedioDeUnEstudiante(listaDeEstudiantes, nombre);
        printf("El promedio para %s es de:  %.2f \n", nombre, promedioEstudiante);
        pausar();
        break;
    default:
        printf("\nOpcion invalida, ingrese nuevamente una opcion \n");
        pausar();
        break;
    }
}