#include "../inc/ListaDeEstudiantes.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

ListaDeEstudiantes *crearListaDeEstudiantes()
{
    ListaDeEstudiantes *nuevaLista = malloc(sizeof(ListaDeEstudiantes));
    nuevaLista->head = NULL;
    nuevaLista->tail = NULL;
    nuevaLista->length = 0;
    return nuevaLista;
}

Estudiante *crearEstudiante(const char *nombre, int edad)
{
    Estudiante *estudiante = malloc(sizeof(Estudiante));
    int i;
    for (i = 0; i < strlen(nombre); i++)
    { // Guarda en Mayus los nombres ingresados, para tenerlo como regla de busqueda.
        estudiante->nombre[i] = toupper(nombre[i]);
    }
    estudiante->nombre[i] = '\0';
    estudiante->edad = edad;
    ListaDeMaterias *listaDeMaterias = nuevaListaDeMaterias();
    estudiante->materias = listaDeMaterias;
    estudiante->next = NULL;
    return estudiante;
}

void generarEstudiantesAleatorios(int cantidadEstudiantes, ListaDeEstudiantes *lista)
{
    FILE *archivo = fopen("nombres.csv", "r");
    if (archivo == NULL)
    {
        printf("No se pudo abrir el archivo.\n");
        return;
    }

    // Leer los nombres del archivo CSV
    char nombres[5000][100];
    int numNombres = 0;
    while (fgets(nombres[numNombres], sizeof(nombres[numNombres]), archivo))
    {
        nombres[numNombres][strlen(nombres[numNombres]) - 1] = '\0'; // Eliminar el salto de línea
        numNombres++;
    }
    fclose(archivo);

    // Generar estudiantes aleatorios
    int estudiantesAgregados = 0;
    while (estudiantesAgregados < cantidadEstudiantes)
    {
        Estudiante *nuevoEstudiante = malloc(sizeof(Estudiante));
        strcpy(nuevoEstudiante->nombre, nombres[rand() % numNombres]);

        // Convertir el nombre a mayúsculas
        for (int j = 0; nuevoEstudiante->nombre[j] != '\0'; j++)
        {
            nuevoEstudiante->nombre[j] = toupper(nuevoEstudiante->nombre[j]);
        }

        nuevoEstudiante->edad = rand() % (35 - 17) + 18;

        // Verificar si el nombre ya está en la lista
        int nombreExistente = 0;
        Estudiante *actual = lista->head;
        while (actual != NULL)
        {
            if (strcmp(actual->nombre, nuevoEstudiante->nombre) == 0)
            {
                nombreExistente = 1;
                break;
            }
            actual = actual->next;
        }

        // Si el nombre no está en la lista, agregar el estudiante
        if (!nombreExistente)
        {
            nuevoEstudiante->materias = malloc(sizeof(ListaDeMaterias));
            nuevoEstudiante->materias->head = NULL;
            nuevoEstudiante->materias->tail = NULL;
            nuevoEstudiante->materias->length = 0;

            nuevoEstudiante->next = NULL;

            append(lista, nuevoEstudiante);
            estudiantesAgregados++;
        }
        else
        {
            // Si el nombre está en la lista, liberar la memoria del estudiante
            free(nuevoEstudiante);
        }
    }
}

void append(ListaDeEstudiantes *listaDeEstudiantes, Estudiante *estudiante)
{

    if (listaDeEstudiantes->head == NULL)
    {
        listaDeEstudiantes->head = estudiante;
        listaDeEstudiantes->tail = estudiante;
    }
    else
    {
        listaDeEstudiantes->tail->next = estudiante;
        listaDeEstudiantes->tail = estudiante;
    }

    listaDeEstudiantes->length++;
}

void removeEstudiante(ListaDeEstudiantes *listaDeEstudiantes, Estudiante *estudiante)
{

    if (listaDeEstudiantes->head == NULL)
    {
        return;
    }

    if (strcmp(listaDeEstudiantes->head->nombre, estudiante->nombre) == 0)
    {
        listaDeEstudiantes->head = listaDeEstudiantes->head->next;
        return;
    }

    Estudiante *current = listaDeEstudiantes->head;

    while (current->next != NULL)
    {
        if (strcmp(current->next->nombre, estudiante->nombre) == 0)
        {
            current->next = current->next->next;
            listaDeEstudiantes->length--;
            return;
        }
        current = current->next;
    }
    free(current);
}

int length(ListaDeEstudiantes *listaDeEstudiantes)
{
    return listaDeEstudiantes->length;
}

Estudiante *buscarEstudiante(ListaDeEstudiantes *listaDeEstudiantes, const char *nombre)
{
    Estudiante *aux = listaDeEstudiantes->head;
    bool encontrado = false;
    Estudiante *estudianteEncontrado = NULL;
    char aux_nombre[50];
    int i;

    memset(aux_nombre, 0x00, sizeof(aux_nombre));
    for (i = 0; i < strlen(nombre); i++)
    {
        aux_nombre[i] = toupper(nombre[i]); // Guarda en una variable aux el nombre ingresado en mayus, para luego compararlo en la busqueda.
    }
    while (aux != NULL)
    {

        if (strcmp(aux->nombre, aux_nombre) == 0)
        {
            printf("%s\n", aux->nombre);
            encontrado = true;
            estudianteEncontrado = aux;
        }
        aux = aux->next;
    }

    return estudianteEncontrado;
}

void listarEstudiantes(ListaDeEstudiantes *listaDeEstudiantes)
{
    int pagina = 1;
    int estudiantesMostrados = 0;
    Estudiante *actual = listaDeEstudiantes->head;

    while (actual != NULL)
    {
        printf("Pagina %d\n", pagina);
        printf("-------------------\n");

        // Mostrar los siguientes 10 estudiantes o menos si no quedan suficientes
        for (int i = 0; i < 10 && actual != NULL; i++)
        {
            printf(" %c %s, %d", 254, actual->nombre, actual->edad);
            printf("\n");
            actual = actual->next;
            estudiantesMostrados++;
        }

        // Verificar si hay más estudiantes en la lista
        if (actual != NULL)
        {
            char opcion;
            printf("\nMostrar los siguientes 10 estudiantes? (s/n): ");
            scanf(" %c", &opcion);

            if (opcion != 's' && opcion != 'S')
            {
                break;
            }
        }

        pagina++;
    }
    printf("\n-------Fin De Pagina-------\n");
    printf("Total de estudiantes mostrados: %d\n", estudiantesMostrados);
}

// Funcion para buscar un estudiante por su nombre y mostrarlo.
Estudiante *buscarEstudianteEdad(ListaDeEstudiantes *listaDeEstudiantes, int edadMin, int edadMax)
{
    int pagina = 1;
    int estudiantesMostrados = 0;
    Estudiante *aux = listaDeEstudiantes->head;
    bool encontrado = false;
    while (aux != NULL)
    {
        printf("Pagina %d\n", pagina);
        printf("-------------------\n");

        for (int i = 0; i < 10 && aux != NULL; i++)
        {
            if (aux->edad >= edadMin && aux->edad < edadMax)
            {
                printf(" %c %s, %d", 254, aux->nombre, aux->edad);
                printf("\n");
                encontrado = true;
            }
            aux = aux->next;
            estudiantesMostrados++;
        }

        if (aux != NULL)
        {
            char opcion;
            printf("\nMostrar los siguientes 10 estudiantes? (s/n): ");
            scanf(" %c", &opcion);

            if (opcion != 's' && opcion != 'S')
            {
                break;
            }
        }
        pagina++;
    }
    if (!encontrado)
    {
        printf("Alumnos no encontrados.\n");
    }
    printf("\n-------Fin De Pagina-------\n");
    printf("Total de estudiantes mostrados: %d\n", estudiantesMostrados);
    return aux;
}

// Ordenar ASC:1, DESC:0
void ordenarNombres(ListaDeEstudiantes *listaDeEstudiantes, int orden)
{
    int intercambiado = 1;

    while (intercambiado)
    {
        intercambiado = 0;
        Estudiante *actual = listaDeEstudiantes->head;
        Estudiante *anterior = NULL;

        while (actual != NULL && actual->next != NULL)
        {
            int comparacion = strcmp(actual->nombre, actual->next->nombre);

            if ((orden == 1 && comparacion > 0) || (orden == 0 && comparacion < 0))
            {
                // Intercambiar los estudiantes
                Estudiante *siguiente = actual->next;

                if (anterior == NULL)
                {
                    listaDeEstudiantes->head = siguiente;
                }
                else
                {
                    anterior->next = siguiente;
                }

                actual->next = siguiente->next;
                siguiente->next = actual;

                if (actual == listaDeEstudiantes->head)
                {
                    listaDeEstudiantes->head = siguiente;
                }

                if (siguiente->next == NULL)
                {
                    listaDeEstudiantes->tail = actual;
                }

                actual = siguiente;
                intercambiado = 1;
            }
            else
            {
                anterior = actual;
                actual = actual->next;
            }
        }
    }
}
// Ordenar ASC:1, DESC:0
void ordenarEdad(ListaDeEstudiantes *listaDeEstudiantes, int orden)
{
    int longitud = listaDeEstudiantes->length;
    int intercambiado;

    do
    {
        intercambiado = 0;
        Estudiante *actual = listaDeEstudiantes->head;
        Estudiante *anterior = NULL;

        while (actual != NULL && actual->next != NULL)
        {
            Estudiante *siguiente = actual->next;

            if ((orden == 1 && actual->edad > siguiente->edad) || (orden == 0 && actual->edad < siguiente->edad))
            {
                // Intercambiar los estudiantes
                if (anterior != NULL)
                {
                    anterior->next = siguiente;
                }
                else
                {
                    listaDeEstudiantes->head = siguiente;
                }
                actual->next = siguiente->next;
                siguiente->next = actual;
                intercambiado = 1;
            }

            anterior = actual;
            actual = siguiente;
        }
    } while (intercambiado);

    // Actualizar el puntero 'tail'
    Estudiante *tail = listaDeEstudiantes->head;
    while (tail->next != NULL)
    {
        tail = tail->next;
    }
    listaDeEstudiantes->tail = tail;
}

ListaDeMaterias *averiguarListaDeMateriasDelEstudiante(Estudiante *estudiante)
{
    return estudiante->materias;
}

void imprimirMateriasDeUnEstudiante(Estudiante *estudiante)
{
    ListaDeMaterias *listaDeMaterias = averiguarListaDeMateriasDelEstudiante(estudiante);
    if (listaDeMaterias != NULL)
    {
        Materia *aux = listaDeMaterias->head;
        printf(" Materias del estudiante: \n");
        while (aux != NULL)
        {
            printf("  %c %s / NOTA: %i\n", 254, aux->nombre, aux->nota);
            aux = aux->proximaMateria;
        }
    }
}

// Verifica si un estudiante esta anotado en una materia
int estudianteEstaAnotado(Estudiante *estudiante, const char *nombreMateria)
{
    Materia *materiaActual = estudiante->materias->head;
    strupr(materiaActual->nombre);
    strupr(nombreMateria);
    while (materiaActual != NULL)
    {

        if (strcmp(materiaActual->nombre, nombreMateria) == 0)
        {
            return 1;
        }
        materiaActual = materiaActual->proximaMateria;
    }
    return 0;
}

// Verifica correlativas del estudiante. Retorna 1 si las correlatividades permiten anotarse a la nueva materia.
int verificarCorrelativas(Estudiante *estudiante, const char *nombreMateria)
{
    if (strcmp(nombreMateria, "ALGEBRA 2") == 0)
    {
        Materia *materia = estudiante->materias->head;
        while (materia != NULL)
        {
            if (strcmp(materia->nombre, "ALGEBRA 1") == 0 && materia->nota >= 4)
            {
                return 1;
            }
            materia = materia->proximaMateria;
        }
    }
    else if (strcmp(nombreMateria, "MATEMATICA DISCRETA") == 0)
    {
        Materia *materia = estudiante->materias->head;
        while (materia != NULL)
        {
            if (strcmp(materia->nombre, "ALGEBRA 2") == 0 && materia->nota >= 4)
            {
                return 1;
            }
            materia = materia->proximaMateria;
        }
    }
    else if (strcmp(nombreMateria, "FISICA 1") == 0)
    {
        Materia *materia = estudiante->materias->head;
        while (materia != NULL)
        {
            if (strcmp(materia->nombre, "ANALISIS MATEMATICO 1") == 0 && materia->nota >= 4)
            {
                return 1;
            }
            materia = materia->proximaMateria;
        }
    }
    else if (strcmp(nombreMateria, "ALGORITMOS 2") == 0)
    {
        Materia *materia = estudiante->materias->head;
        while (materia != NULL)
        {
            if (strcmp(materia->nombre, "ALGORITMOS 1") == 0 && materia->nota >= 4)
            {
                return 1;
            }
            materia = materia->proximaMateria;
        }
    }
    else if (strcmp(nombreMateria, "ESTRUCTURA DE DATOS") == 0)
    {
        Materia *materia = estudiante->materias->head;
        while (materia != NULL)
        {
            if (strcmp(materia->nombre, "ALGORITMOS 2") == 0 && materia->nota >= 4)
            {
                return 1;
            }
            materia = materia->proximaMateria;
        }
    }
    else if (strcmp(nombreMateria, "MATEMATICAS ESPECIALES") == 0)
    {
        Materia *materia = estudiante->materias->head;
        while (materia != NULL)
        {
            if (strcmp(materia->nombre, "ANALISIS MATEMATICO 2") == 0 && materia->nota >= 4)
            {
                return 1;
            }
            materia = materia->proximaMateria;
        }
    }
    else if (strcmp(nombreMateria, "ARQUITECTURA") == 0)
    {
        Materia *materia = estudiante->materias->head;
        while (materia != NULL)
        {
            if (strcmp(materia->nombre, "DISENIO LOGICO") == 0 && materia->nota >= 4)
            {
                return 1;
            }
            materia = materia->proximaMateria;
        }
    }
    else if (strcmp(nombreMateria, "ALGORITMOS 3") == 0)
    {
        Materia *materia = estudiante->materias->head;
        while (materia != NULL)
        {
            if (strcmp(materia->nombre, "ESTRUCTURA DE DATOS") == 0 && materia->nota >= 4)
            {
                return 1;
            }
            materia = materia->proximaMateria;
        }
    }
    else if (strcmp(nombreMateria, "ANALISIS MATEMATICO 2") == 0)
    {
        Materia *materia1 = estudiante->materias->head;
        int tieneMateria1 = 0;
        Materia *materia2 = estudiante->materias->head;
        int tieneMateria2 = 0;

        while (materia1 != NULL && materia2 != NULL)
        {
            if (strcmp(materia1->nombre, "ALGEBRA 2") == 0 && materia1->nota >= 4)
            {
                tieneMateria1 = 1;
            }
            if (strcmp(materia2->nombre, "ANALISIS MATEMATICO 1") == 0 && materia2->nota >= 4)
            {
                tieneMateria2 = 1;
            }

            if (tieneMateria1 && tieneMateria2)
            {
                return 1;
            }

            materia1 = materia1->proximaMateria;
            materia2 = materia2->proximaMateria;
        }
    }
    else if (strcmp(nombreMateria, "FISICA 2") == 0)
    {
        Materia *materia1 = estudiante->materias->head;
        int tieneMateria1 = 0;
        Materia *materia2 = estudiante->materias->head;
        int tieneMateria2 = 0;

        while (materia1 != NULL && materia2 != NULL)
        {
            if (strcmp(materia1->nombre, "ANALISIS MATEMATICO 2") == 0 && materia1->nota >= 4)
            {
                tieneMateria1 = 1;
            }
            if (strcmp(materia2->nombre, "FISICA 1") == 0 && materia2->nota >= 4)
            {
                tieneMateria2 = 1;
            }

            if (tieneMateria1 && tieneMateria2)
            {
                return 1;
            }

            materia1 = materia1->proximaMateria;
            materia2 = materia2->proximaMateria;
        }
    }
    else if (strcmp(nombreMateria, "DISENIO LOGICO") == 0)
    {
        Materia *materia1 = estudiante->materias->head;
        int tieneMateria1 = 0;
        Materia *materia2 = estudiante->materias->head;
        int tieneMateria2 = 0;

        while (materia1 != NULL && materia2 != NULL)
        {
            if (strcmp(materia1->nombre, "ALGEBRA 1") == 0 && materia1->nota >= 4)
            {
                tieneMateria1 = 1;
            }
            if (strcmp(materia2->nombre, "SISTEMAS DE REPRESENTACION") == 0 && materia2->nota >= 4)
            {
                tieneMateria2 = 1;
            }

            if (tieneMateria1 && tieneMateria2)
            {
                return 1;
            }

            materia1 = materia1->proximaMateria;
            materia2 = materia2->proximaMateria;
        }
    }
    else if (strcmp(nombreMateria, "CULTURA") == 0 || strcmp(nombreMateria, "CUESTIONES") == 0 ||
             strcmp(nombreMateria, "HISTORIA") == 0 || strcmp(nombreMateria, "PROBLEMATICAS") == 0 ||
             strcmp(nombreMateria, "ALGEBRA 1") == 0 || strcmp(nombreMateria, "ANALISIS MATEMATICO 1") == 0 ||
             strcmp(nombreMateria, "ALGORITMOS 1") == 0 || strcmp(nombreMateria, "SISTEMAS DE REPRESENTACION") == 0)
    {
        return 1; // No se verifica correlativas para estas materias
    }
    return 0;
}

int cantidadDeAnotadosEnUnaMateria(ListaDeEstudiantes *listaDeEstudiantes, char *nombreDeMateria)
{
    int contador = 0;
    Estudiante *aux = listaDeEstudiantes->head;
    strupr(nombreDeMateria);
    while (aux != NULL)
    {
        Materia *materia = aux->materias->head;
        while (materia != NULL)
        {
            if (strcmp(materia->nombre, nombreDeMateria) == 0)
            {
                contador++;
            }
            materia = materia->proximaMateria;
        }
        aux = aux->next;
    }
    return contador;
}

int cantidadDeAprobadosEnUnaMateria(ListaDeEstudiantes *listaDeEstudiantes, char *nombreDeMateria)
{
    int contador = 0;
    Estudiante *aux = listaDeEstudiantes->head;
    strupr(nombreDeMateria);
    while (aux != NULL)
    {
        Materia *materia = aux->materias->head;
        while (materia != NULL)
        {
            if (strcmp(materia->nombre, nombreDeMateria) == 0)
            {
                if (materia->nota >= 4)
                {
                    contador++;
                }
            }
            materia = materia->proximaMateria;
        }
        aux = aux->next;
    }
    return contador;
}

int cantidadDeDesaprobadosEnUnaMateria(ListaDeEstudiantes *listaDeEstudiantes, char *nombreDeMateria)
{
    int contador = 0;
    Estudiante *aux = listaDeEstudiantes->head;
    strupr(nombreDeMateria);
    while (aux != NULL)
    {
        Materia *materia = aux->materias->head;
        while (materia != NULL)
        {
            if (strcmp(materia->nombre, nombreDeMateria) == 0)
            {
                if (materia->nota < 4)
                {
                    contador++;
                }
            }
            materia = materia->proximaMateria;
        }
        aux = aux->next;
    }
    return contador;
}

float promedioDeNotasDeUnaMateria(ListaDeEstudiantes *listaDeEstudiantes, char *nombreDeMateria)
{
    int contador = 0;
    int suma = 0;

    Estudiante *aux = listaDeEstudiantes->head;
    strupr(nombreDeMateria);
    while (aux != NULL)
    {
        Materia *auxMateria = aux->materias->head;
        while (auxMateria != NULL)
        {
            if (strcmp(auxMateria->nombre, nombreDeMateria) == 0)
            {
                suma += auxMateria->nota;
                contador++;
            }
            auxMateria = auxMateria->proximaMateria;
        }
        aux = aux->next;
    }
    return suma / contador;
}

float promedioDeUnEstudiante(ListaDeEstudiantes *listaDeEstudiantes, char *nombreDeEstudiante)
{
    int contador = 0;
    int suma = 0;

    Estudiante *aux = listaDeEstudiantes->head;
    strupr(nombreDeEstudiante);
    while (aux != NULL)
    {
        if (strcmp(aux->nombre, nombreDeEstudiante) == 0)
        {
            Materia *auxMateria = aux->materias->head;
            while (auxMateria != NULL)
            {
                suma += auxMateria->nota;
                contador++;
                auxMateria = auxMateria->proximaMateria;
            }
            break;
        }
            aux = aux->next;
    }
    return suma / contador;
}
