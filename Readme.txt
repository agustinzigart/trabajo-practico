Integrantes Grupo FIZZBUZZ:

* Lacomi Agustín
* Macen Franco
* Mulé David
* Mulé Gaspar
* Zigart Agustín

Puntos obligatorios hechos:
    -Dar de alta y listar estudiantes 
    -Buscar estudiantes por nombre
    -Buscar estudiantes por rango de edad
    -Además cada estudiante puede anotarse en N materias.
    -Dar de alta materia
    -Listar materias
    -Anotarse en una materia
    -Rendir una materia


Tareas extras hechas:
    -mejoras en la interfaz de usuario
    -Paginado en el menú 
    -Generación aleatorias de estudiantes y materias aleatorias (revisar cantidad)
    -ordenamiento del listado de estudiantes por nombre
    -ordenamiento del listado de estudiantes por edad
    -archivo de configuración general donde se especifican las variables del sistema.
    -Calcular promedio
    -qué pasa si una materia anterior está desaprobada? Puede anotarse?
    -Muestra de materias y correlaciones
    -Pruebas unitarias (agregar algunas mas)
    -estadisticas de los estudiantes y materias, etc. 
    
Para ejecutar los tests:
    Los mismos se encuentran en unitTests.c, para compilacion y ejecucion de los mismos ejecutar en consola de powershell:
    > gcc unitTests.c .\functions\ListaDeEstudiantes.c .\functions\MateriasFunciones.c .\functions\menu.c -o unitTests
    > .\unitTests